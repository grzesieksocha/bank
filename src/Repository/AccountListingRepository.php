<?php

namespace App\Repository;

use App\Entity\AccountListing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AccountListing|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountListing|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountListing[]    findAll()
 * @method AccountListing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountListingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccountListing::class);
    }

    // /**
    //  * @return AccountListing[] Returns an array of AccountListing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccountListing
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
